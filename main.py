import kivy
print("v", kivy.__version__ )
from kivy.app import App
from kivy.uix.stacklayout import StackLayout
from kivy.uix.textinput import TextInput
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.scrollview import ScrollView




class MyLayout(StackLayout):
    def __init__(self, **kwargs):
        super(MyLayout, self).__init__(**kwargs)
        
        self.size_hint_y = None
        self.bind(minimum_height=self.setter('height'))
        
        self.welcome = Label(text="Hello Ken! how many trays do you have?",
                            height=50, width=100,
                            size_hint=(1, None),
                            color=(0 ,0, 1, 1)
                            )
        self.add_widget(self.welcome)
        
        self.orientation = 'lr-tb'
        
        self.input = TextInput(input_type= 'number',
                                height=75, width=100,
                                size_hint=(1, None),
                                )
        thing1 = self.add_widget(self.input)
        
        self.seeds = Label(text="How much do your seeds way in total?",
                            height=50, width=100,
                            size_hint=(1, None),
                            color=(0 ,0, 1, 1))
        self.add_widget(self.seeds)
        
        self.input1 = TextInput(input_type = 'number',
                                height=75,
                                width=100, 
                                size_hint=(1, None),
                                )
        thing2 = self.add_widget(self.input1)

        self.calculate = Button(text='Calculate', height=75, width=100, size_hint=(1, None),
                                on_press=self.convert_to_int,
                                background_color=(0, 1, 0, 1)
                                )
        self.add_widget(self.calculate)
        
        self.removeQuantities = Button(text='Remove', height=75, width=100, size_hint=(1, None),
                                on_press=self.remove,
                                background_color=(1, 0, 0, 1)
                                )
        self.add_widget(self.removeQuantities)

    def convert_to_int(self, instance):
        try:
            value = int(self.input.text)
            value1 = int(self.input1.text)
            perTray = round(value1/value, 2)
            remainder = round(value1-perTray, 2)
            areLabels = False
                        
            perTrayLabel = Label(text= f'You will need {perTray} oz of seed per tray.',
                                height=50, 
                                width=100, 
                                size_hint=(1, None),
                                color=(0 ,0, 1, 1))
            self.add_widget(perTrayLabel)
            
            for i in range(1, value +1):
                outputLabel = Label(text=f"After tray {i} you will have {round(remainder, 2)} grams of seed left",
                    height=50, width=100, size_hint=(1, None),
                    color=(0 ,0, 1, 1)
                    )
                self.add_widget(outputLabel)
                remainder -= round(perTray, 2)
            areLabels == True
            
        except ValueError:
            print('Invalid input')
            
    def remove(self, instance):
        self.clear_widgets(self.children[:-6])
        print("accessed")
    
class MyApp(App):
    def build(self):
        scroll_view = ScrollView()
        layout = MyLayout()
        scroll_view.add_widget(layout)
        return scroll_view

if __name__ == '__main__':
    MyApp().run()